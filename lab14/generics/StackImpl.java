package stack;

import java.util.ArrayList;

public class StackImpl <T> implements Stack <T> {
    StackItem <T> top=null;


    @Override
    public void push(T item) {
        StackItem <T> box = new StackItem<>(item);
        StackItem <T> previousTop = top;
        top = box;
        box.setNext(previousTop);

    }

    @Override
    public void push(Object item) {

    }

    @Override
    public T pop() {
        StackItem<> oldTop = top;
        top=top.getNext();
        return oldTop.getItem();
    }

    @Override
    public boolean empty() {
        return top==null;
    }
    @Override
    public List<T> toList(){
        ArrayList<T> list = new ArrayList<>();
        StackItem<T> item =top;
        while (item!null){
            list.add(item.getitem());
            item= item.getNext();
        }
        return list;
    }

    @Override
    public void addAll(Stack <T> aStack){
        List values = aStack.toList();
        for(T value: values){
            push(value);
        }
    }
}
